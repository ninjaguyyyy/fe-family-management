const MEAL_ADD = 'MEAL_ADD';
const MEAL_REMOVE = 'MEAL_REMOVE';

export const mealTypes = {
    MEAL_REMOVE,
    MEAL_ADD,
};
