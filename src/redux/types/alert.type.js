const ALERT_OPEN = "ALERT_OPEN";
const ALERT_CLOSE = "ALERT_CLOSE";

export const alertTypes = {
    ALERT_OPEN,
    ALERT_CLOSE,
};
