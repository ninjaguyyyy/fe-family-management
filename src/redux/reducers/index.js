import { combineReducers } from 'redux';

import alert from './alert.reducer';
import meals from './meal.reducer';

const rootReducer = combineReducers({
    alert,
    meals,
});

export default rootReducer;
