import { mealTypes } from '../types';

const initialState = [];

export default function MealReducer(state = initialState, action) {
    switch (action.type) {
        case mealTypes.MEAL_ADD: {
            return [...state, { ...action.payload }];
        }
        case mealTypes.MEAL_REMOVE: {
            return [];
        }
        default: {
            return state;
        }
    }
}
