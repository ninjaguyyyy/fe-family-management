import { alertTypes } from "../types";

const initialState = { open: false, content: null, type: null };

export default function AlertReducer(state = initialState, action) {
    switch (action.type) {
        case alertTypes.ALERT_OPEN: {
            return { ...state, open: true, ...action.payload };
        }
        case alertTypes.ALERT_CLOSE: {
            return { open: false };
        }
        default: {
            return state;
        }
    }
}
