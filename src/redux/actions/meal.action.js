import { mealTypes } from '../types';

export const mealAction = {
    add,
    remove,
};

function add(id, image, name) {
    return {
        type: mealTypes.MEAL_ADD,
        payload: { id, image, name },
    };
}

function remove(id) {
    return {
        type: mealTypes.MEAL_REMOVE,
        payload: { id },
    };
}
