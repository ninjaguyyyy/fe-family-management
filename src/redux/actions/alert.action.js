import { alertTypes } from "../types";

export const alertAction = {
    open,
    close,
};

function open(content, type) {
    return {
        type: alertTypes.ALERT_OPEN,
        payload: { content, type },
    };
}

function close() {
    return {
        type: alertTypes.ALERT_CLOSE,
    };
}
