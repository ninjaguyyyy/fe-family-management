import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { useSelector, useDispatch } from 'react-redux';

import Header from './header';
import Sidebar from './sidebar';
import { alertAction } from '../../redux/actions';

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(8),
    marginLeft: theme.spacing(8),
    backgroundColor: '#eee',
    height: '100%',
  },
}));

export default function Layout(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [openDrawer, setOpenDrawer] = useState(false);

  const alert = useSelector((state) => state.alert);

  const handleCloseAlert = () => {
    dispatch(alertAction.close());
  };

  const handleDrawerOpen = () => {
    setOpenDrawer(true);
  };
  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  return (
    <div style={{ height: '100%' }}>
      <Header openDrawer={openDrawer} handleDrawerOpen={handleDrawerOpen} />
      <Sidebar openDrawer={openDrawer} handleDrawerClose={handleDrawerClose} />
      <main className={classes.layout}>{props.children}</main>
      <Snackbar
        open={alert.open}
        autoHideDuration={6000}
        onClose={handleCloseAlert}
      >
        <Alert onClose={handleCloseAlert} severity={alert.type}>
          {alert.content}
        </Alert>
      </Snackbar>
    </div>
  );
}
