import axiosClient from './axiosClient';

const url = '/expense';

export const ExpenseService = {
    get,
    createExpense,
};

function get(params) {
    return axiosClient.get(url, { params });
}

function createExpense({ user, content, cost, month, year }) {
    return axiosClient.post(url, { user, cost, content, month, year });
}
