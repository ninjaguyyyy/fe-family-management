export const countHelper = {
    countCostEachUser,
};

function countCostEachUser(data) {
    let filteredData0 = data.filter((item, index) => item.user === 'chi');
    let filteredData1 = data.filter((item, index) => item.user === 'chung');
    let filteredData2 = data.filter((item, index) => item.user === 'nhat');

    let count0 = filteredData0.reduce(
        (prevValue, curr) => prevValue + curr.cost,
        0
    );
    let count1 = filteredData1.reduce(
        (prevValue, curr) => prevValue + curr.cost,
        0
    );
    let count2 = filteredData2.reduce(
        (prevValue, curr) => prevValue + curr.cost,
        0
    );
    return [
        { user: 'chi', cost: count0 },
        { user: 'chung', cost: count1 },
        { user: 'nhat', cost: count2 },
    ];
}
