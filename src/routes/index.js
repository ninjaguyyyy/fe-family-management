import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { routes } from "./define.routes";
import Layout from "../components/layout";

export default function Routes() {
    return (
        <Router>
            <Layout>
                <Switch>
                    {routes.map((route, index) => (
                        <Route
                            key={index}
                            exact
                            path={route.path}
                            component={route.component}
                        ></Route>
                    ))}
                </Switch>
            </Layout>
        </Router>
    );
}
