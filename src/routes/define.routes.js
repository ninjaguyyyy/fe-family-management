import React from 'react';
import { Redirect } from 'react-router-dom';
import Expense from '../pages/expense';
import Statistics from '../pages/statistics';
import Meals from '../pages/meals';

export const routes = [
    { path: '/', component: () => <Redirect to="/expense" />, auth: false },
    { path: '/expense', component: () => <Expense />, auth: false },
    { path: '/costs', component: () => <Statistics />, auth: false },
    { path: '/meals', component: () => <Meals />, auth: false },
];
