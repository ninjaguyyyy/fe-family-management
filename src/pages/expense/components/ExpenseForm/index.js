import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const users = [
    {
        id: '1',
        name: 'chi',
    },
    {
        id: '2',
        name: 'chung',
    },
    {
        id: '3',
        name: 'nhat',
    },
];

export default function ExpenseForm({ handleSubmit }) {
    const [user, setUser] = React.useState('chi');
    const [cost, setCost] = React.useState(null);
    const [content, setContent] = React.useState('');

    const handleChangeSelect = (event) => {
        setUser(event.target.value);
    };

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
                <TextField
                    id="outlined-select-currency"
                    select
                    label="Người trả"
                    value={user}
                    onChange={handleChangeSelect}
                    variant="outlined"
                    fullWidth
                >
                    {users.map((option) => (
                        <MenuItem key={option.id} value={option.name}>
                            {option.name}
                        </MenuItem>
                    ))}
                </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    id="cost"
                    value={cost}
                    onChange={(e) => setCost(e.target.value)}
                    name="cost"
                    label="Số tiền (đv: k)"
                    fullWidth
                    autoComplete="family-name"
                    variant="outlined"
                />
            </Grid>
            <Grid item xs={12} sm={12}>
                <TextField
                    id="outlined-multiline-static"
                    label="Nội dung"
                    value={content}
                    onChange={(e) => setContent(e.target.value)}
                    multiline
                    rows={4}
                    variant="outlined"
                    fullWidth
                />
            </Grid>
            <div style={{ margin: 'auto', marginBottom: '10px' }}>
                <Button
                    variant="contained"
                    size="large"
                    color="primary"
                    onClick={() => handleSubmit({ user, cost, content })}
                >
                    Xác nhận
                </Button>
            </div>
        </Grid>
    );
}
