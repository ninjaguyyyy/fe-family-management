import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';

import ExpenseForm from './components/ExpenseForm';
import { ExpenseService } from '../../services';
import { alertAction } from '../../redux/actions';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        width: 600,
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
        height: 'fit-content',
    },
    divider: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    action: {
        display: 'flex',
        justifyContent: 'center',
    },
    margin: {
        marginTop: theme.spacing(3),
        // marginBottom: theme.spacing(3),
    },
}));

export default function Expense() {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    const handleCreate = ({ user, cost, content }) => {
        (async function () {
            let dateNow = new Date();
            setLoading(true);
            const result = await ExpenseService.createExpense({
                month: dateNow.getMonth() + 1,
                year: dateNow.getFullYear(),
                user,
                cost,
                content,
            });
            if (result.success) {
                setLoading(false);
                dispatch(alertAction.open('Đã thêm thành công.', 'success'));
            }
        })();
    };

    return (
        <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center">
                Chi tiêu sinh hoạt
            </Typography>
            <Divider className={classes.divider} />
            <ExpenseForm handleSubmit={handleCreate} />
            {loading && (
                <div
                    style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        marginTop: '15px',
                    }}
                >
                    <CircularProgress />
                </div>
            )}
        </Paper>
    );
}
