import DateFnsUtils from '@date-io/date-fns';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import Link from '@material-ui/core/Link';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import GrainIcon from '@material-ui/icons/Grain';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { countHelper } from '../../helper';
import { ExpenseService } from '../../services';

const useStyles = makeStyles((theme) => ({
  table: {
    width: '100%',
    border: '1px solid #ccc',
  },
  paper: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: theme.spacing(7),
    marginRight: theme.spacing(7),
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(6),
  },
  link: {
    display: 'flex',
  },
  tableHead: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  statusYes: {
    padding: '5px 12px',
    backgroundColor: '#3bca3b',
    color: '#fff',
    borderRadius: '12px',
  },
  statusNo: {
    padding: '5px 12px',
    backgroundColor: 'red',
    color: '#fff',
    borderRadius: '12px',
  },
  actions: {
    display: 'flex',
  },
  margin: {
    marginLeft: theme.spacing(3),
  },
}));

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export default function Statistics() {
  const classes = useStyles();
  const history = useHistory();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [rowsPerPage, setRowsPerPage] = React.useState(2);
  const [page, setPage] = React.useState(0);
  const [data, setData] = useState([]);

  const goTo = (path) => history.push(path);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeMonth = (date) => {
    setSelectedDate(date);
  };

  useEffect(() => {
    (async function () {
      const result = await ExpenseService.get({
        month: selectedDate.getMonth() + 1,
        year: selectedDate.getFullYear(),
      });
      setData(result);

      let countEach = countHelper.countCostEachUser(result.expenseDetails);
      setData((state) => ({ ...state, countEach }));
    })();
    return () => {
      // cleanup
    };
  }, [selectedDate]);

  return (
    <Paper className={classes.paper}>
      <Grid container spacing={3}>
        <Grid
          item
          xs={6}
          sm={6}
          md={6}
          lg={8}
          className="d-flex justify-content-start"
        >
          <Grid container spacing={2}>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              className="d-flex justify-content-start"
            >
              <Breadcrumbs aria-label="breadcrumb">
                <Link
                  href="#"
                  color="inherit"
                  onClick={() => goTo('/')}
                  className={classes.link}
                  style={{ textDecoration: 'none' }}
                >
                  <HomeIcon className={classes.icon1} />
                  Trang chủ
                </Link>
                <Typography color="textPrimary" className={classes.link}>
                  <GrainIcon className={classes.icon1} /> Danh sách chi tiêu
                </Typography>
              </Breadcrumbs>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={4}>
          <FormControl variant="outlined" fullWidth margin="dense">
            <InputLabel htmlFor="outlined-adornment">Tìm kiếm</InputLabel>
            <OutlinedInput
              id="outlined-adornment"
              endAdornment={
                <InputAdornment position="end">
                  <SearchIcon color="primary" />
                </InputAdornment>
              }
              labelWidth={70}

              // onChange={(e)=> setQ(e.target.value)}
            />
          </FormControl>
        </Grid>
      </Grid>
      <div className="tables" style={{ marginTop: '2%' }}>
        <div className={classes.actions}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              variant="inline"
              inputVariant="outlined"
              openTo="month"
              views={['year', 'month']}
              label="Year and Month"
              value={selectedDate}
              onChange={handleChangeMonth}
            />
          </MuiPickersUtilsProvider>
          <Button
            variant="contained"
            size="large"
            color="primary"
            className={classes.margin}
          >
            Thanh toán
          </Button>
        </div>

        <Grid container spacing={3} style={{ marginTop: '0.5%' }}>
          <Grid item sm={8}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Thời gian</StyledTableCell>
                  <StyledTableCell align="right">Nội dung</StyledTableCell>
                  <StyledTableCell align="right">
                    Số tiền&nbsp;(k)
                  </StyledTableCell>
                  <StyledTableCell align="right">Người trả</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.expenseDetails?.length !== undefined ? (
                  data.expenseDetails.map((row) => (
                    <StyledTableRow key={row.id}>
                      <StyledTableCell component="th" scope="row">
                        {row.createdAt}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.content}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.cost}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.user}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
                ) : (
                  <TableCell colSpan={4} align="center">
                    <CircularProgress />
                  </TableCell>
                )}
                {}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[
                      5,
                      10,
                      25,
                      { label: 'All', value: -1 },
                    ]}
                    colSpan={4}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    // ActionsComponent={Statistics}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </Grid>
          <Grid item sm={4}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Tổng</StyledTableCell>
                  <StyledTableCell>{data.total}k</StyledTableCell>
                  <StyledTableCell align="right">
                    <span className={classes.statusYes}>rồi</span>
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.countEach
                  ? data.countEach.map((item) => (
                      <StyledTableRow key={item.user}>
                        <StyledTableCell>{item.user}</StyledTableCell>
                        <StyledTableCell colSpan="2">
                          {item.cost}k
                        </StyledTableCell>
                      </StyledTableRow>
                    ))
                  : 'b'}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </div>
    </Paper>
  );
}
