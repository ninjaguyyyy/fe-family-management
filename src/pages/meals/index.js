import React from 'react';
import MealTab from './components/MealTab';
import TrayContainer from './components/TrayContainer';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

export default function Meals() {
    return (
        <DndProvider backend={HTML5Backend}>
            <div
                style={{
                    width: '100%',
                    padding: '40px 40px 20px 40px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    boxSizing: 'border-box',
                }}
            >
                <MealTab />
                <TrayContainer />
            </div>
        </DndProvider>
    );
}
