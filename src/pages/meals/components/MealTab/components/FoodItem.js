import React from 'react';
import './FoodItem.css';
import { useDrag } from 'react-dnd';
import { ItemTypes } from '../../../../../constants/drag_drop';

export default function FoodItem({ url, style, name }) {
  const [{ isDragging }, drag] = useDrag({
    item: {
      type: ItemTypes.FOOD,
      id: 'nho_them_vo_1',
      image: { url },
      name: 'them nha',
    },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  });

  return (
    <div style={{ display: 'inline-flex', flexDirection: 'column' }}>
      <img
        alt="img"
        src={url}
        className="FoodItem"
        ref={drag}
        style={{
          ...style,
          opacity: isDragging ? 0.5 : 1,
          cursor: 'grab',
        }}
      ></img>
      <span
        style={{
          fontSize: '12px',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {name}
      </span>
    </div>
  );
}
