import {
  faAppleAlt,
  faHamburger,
  faSeedling,
  faTint,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import React from 'react';
import FoodItem from './components/FoodItem';

const foods_main = [
  {
    id: '1',
    name: 'thịt kho tiêu',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608732285/manage_family/New_Project_6_q6c0cc.png',
  },
  {
    id: '1',
    name: 'cá mó chiên',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608731908/manage_family/New_Project_5_i2odad.png',
  },
  {
    id: '1',
    name: 'cá chiên',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608731642/manage_family/New_Project_4_xo0cgv.png',
  },
  {
    id: '1',
    name: 'cơm',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608731268/manage_family/New_Project_3_fx5rhm.png',
  },
  {
    id: '1',
    name: 'thịt kho tiêu',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608732285/manage_family/New_Project_6_q6c0cc.png',
  },
  {
    id: '1',
    name: 'trứng đổ chả',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608824199/manage_family/New_Project_7_kxn32m.png',
  },
  {
    id: '1',
    name: 'trứng opla',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608824407/manage_family/New_Project_8_h9bsm8.png',
  },
  {
    id: '1',
    name: 'cóc lết rim',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608824676/manage_family/New_Project_9_h4vowj.png',
  },
  {
    id: '1',
    name: 'sườn non kho',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608824846/manage_family/New_Project_10_ztakar.png',
  },
  {
    id: '1',
    name: 'thịt kho trứng',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608825069/manage_family/New_Project_11_pdfokk.png',
  },
  {
    id: '1',
    name: 'đậu nhồi thịt',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608825268/manage_family/New_Project_12_bius7t.png',
  },
  {
    id: '1',
    name: 'đậu sốt cà',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608825429/manage_family/New_Project_13_f5eutx.png',
  },
  {
    id: '1',
    name: 'cá kho',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608828739/manage_family/New_Project_23_mu26tz.png',
  },
  {
    id: '1',
    name: 'cơm chiên',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159297/manage_family/132891941_542370573385399_6877288751209714834_n_gencbt.png',
  },
  {
    id: '1',
    name: 'mì xào',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159299/manage_family/132785925_1517717661758724_4927287469449062294_n_car8iq.png',
  },
  {
    id: '1',
    name: 'trứng vịt luộc',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159300/manage_family/132428257_220842089571476_4355304531309803567_n_a9fc8w.png',
  },
];

const foods_soup = [
  {
    id: '1',
    name: 'canh bầu',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608723061/manage_family/New_Project_agv5u9.png',
  },
  {
    id: '1',
    name: 'canh mướp',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608825919/manage_family/New_Project_16_duskzq.png',
  },
  {
    id: '1',
    name: 'canh giò heo',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608828308/manage_family/New_Project_22_cr271a.png',
  },
  {
    id: '1',
    name: 'canh bí đỏ',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608828205/manage_family/New_Project_21_rs4iu2.png',
  },
  {
    id: '1',
    name: 'mồng tơi',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159297/manage_family/132938680_181732860303129_3411759800571176690_n_nwy2kg.png',
  },
  {
    id: '1',
    name: 'tần ô',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159298/manage_family/131989285_113464810548840_5938064754911921382_n_c1iquv.png',
  },
  {
    id: '1',
    name: 'canh chua tôm',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159298/manage_family/132002386_2510263222599412_3350501621459913768_n_gycel1.png',
  },
  {
    id: '1',
    name: 'canh cải thảo',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159299/manage_family/131662593_183666096803426_1892334527928383536_n_xornlf.png',
  },
  {
    id: '1',
    name: 'canh cà trứng',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159299/manage_family/131662593_183666096803426_1892334527928383536_n_xornlf.png',
  },
  {
    id: '1',
    name: 'canh cải ngọt',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159299/manage_family/131662593_183666096803426_1892334527928383536_n_xornlf.png',
  },
];

const foods_vegetable = [
  {
    id: '1',
    name: 'rau muống xào',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608723511/manage_family/New_Project_1_mzrmld.png',
  },
  {
    id: '1',
    name: 'rau lang luộc',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608825531/manage_family/New_Project_14_zzpcag.png',
  },
  {
    id: '1',
    name: 'mồng tơi luộc',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608825690/manage_family/New_Project_15_vhjea6.png',
  },
  {
    id: '1',
    name: 'rau sống',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159297/manage_family/131997778_1138918276549962_8998746128701210800_n_m0nlrg.png',
  },
  {
    id: '1',
    name: 'khổ qua xàotrứng',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159298/manage_family/132259419_417380266052992_5683539323036035482_n_l6fjag.png',
  },
  {
    id: '1',
    name: 'đậu bắp luộc',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159298/manage_family/132336766_1124164444677721_1169711642468832247_n_sp6jjn.png',
  },
  {
    id: '1',
    name: 'bắp cải xào',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159299/manage_family/133659893_146905106974894_6704531369604490563_n_tejfem.png',
  },
  {
    id: '1',
    name: 'cà tím nướng',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159299/manage_family/132963449_2723554034574872_8180234845961661647_n_d0yg6i.png',
  },
];

const foods_final = [
  {
    id: '1',
    name: 'dưa hấu',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608723836/manage_family/New_Project_2_ef6zjc.png',
  },
  {
    id: '1',
    name: 'quýt',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608827469/manage_family/New_Project_17_eenaet.png',
  },
  {
    id: '1',
    name: 'dừa',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608827671/manage_family/New_Project_18_cshbp1.png',
  },
  {
    id: '1',
    name: 'bưởi',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608827853/manage_family/New_Project_19_dce0lq.png',
  },
  {
    id: '1',
    name: 'nước mía',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159296/manage_family/132891123_2704922566436741_789232098135271342_n_rx2wtr.png',
  },
  {
    id: '1',
    name: 'chuối',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1608827948/manage_family/New_Project_20_fmavf6.png',
  },
  {
    id: '1',
    name: 'rau má',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159296/manage_family/133264876_881890579240460_2061158414025000148_n_zl2cce.png',
  },
  {
    id: '1',
    name: 'cà dằm',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159296/manage_family/133118707_2457057734601467_6347253713679931573_n_ntvjpt.png',
  },
  {
    id: '1',
    name: 'nước chanh',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159297/manage_family/133577446_220986259522356_1957509574340344736_n_lfald1.png',
  },
  {
    id: '1',
    name: 'chè bưởi',
    image:
      'https://res.cloudinary.com/dwuma83gt/image/upload/v1609159297/manage_family/131569235_401806514464697_3004462581154888578_n_piinkr.png',
  },
];

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '30%',
    backgroundColor: theme.palette.background.paper,
  },
  tab: {
    minWidth: '60px',
    fontSize: '13px',
  },
}));

export default function MealTab() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Paper square>
        <Tabs
          value={value}
          onChange={handleChange}
          variant="fullWidth"
          tabItemContainerStyle={{ width: '50px' }}
          indicatorColor="primary"
          textColor="primary"
          aria-label="icon tabs example"
        >
          <Tab
            className={classes.tab}
            icon={<FontAwesomeIcon icon={faHamburger} size="2x" />}
            aria-label="phone"
            label="Chính"
          />
          <Tab
            icon={<FontAwesomeIcon icon={faTint} size="2x" />}
            aria-label="favorite"
            className={classes.tab}
            label="Canh"
          />
          <Tab
            icon={<FontAwesomeIcon icon={faSeedling} size="2x" />}
            aria-label="person"
            label="Rau củ"
            className={classes.tab}
          />
          <Tab
            icon={<FontAwesomeIcon icon={faAppleAlt} size="2x" />}
            aria-label="person"
            label="T miệng"
            className={classes.tab}
          />
        </Tabs>
      </Paper>

      <TabPanel value={value} index={0}>
        <div>
          {foods_main?.map((food) => (
            <FoodItem
              style={{ margin: '0 12px' }}
              url={food.image}
              name={food.name}
            />
          ))}
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <div>
          {foods_soup?.map((food) => (
            <FoodItem
              style={{ margin: '0 12px' }}
              url={food.image}
              name={food.name}
            />
          ))}
        </div>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <div>
          {foods_vegetable?.map((food) => (
            <FoodItem
              style={{ margin: '0 12px' }}
              url={food.image}
              name={food.name}
            />
          ))}
        </div>
      </TabPanel>
      <TabPanel value={value} index={3}>
        <div>
          {foods_final?.map((food) => (
            <FoodItem
              style={{ margin: '0 12px' }}
              url={food.image}
              name={food.name}
            />
          ))}
        </div>
      </TabPanel>
    </div>
  );
}
