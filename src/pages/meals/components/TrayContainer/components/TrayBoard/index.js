import React from 'react';
import { useDrop } from 'react-dnd';
import { ItemTypes } from '../../../../../../constants/drag_drop';
import { useSelector, useDispatch } from 'react-redux';
import { mealAction } from '../../../../../../redux/actions';

export default function TrayBoard() {
  const meals = useSelector((state) => state.meals);
  const dispatch = useDispatch();

  const [drop] = useDrop({
    accept: ItemTypes.FOOD,
    drop: (item, monitor) => {
      dispatch(mealAction.add(item.id, item.image.url, item.name));
    },
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  });

  return (
    <div
      style={{
        height: '500px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <div
        ref={drop}
        style={{
          width: '750px',
          height: '450px',
          backgroundSize: 'cover',
          backgroundImage:
            'url(' +
            'https://res.cloudinary.com/dwuma83gt/image/upload/v1608722550/manage_family/2.-CT-6253-NT-Soi_al4m9u.jpg' +
            ')',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexWrap: 'wrap',
            width: '700px',
            paddingTop: '20px',
            height: '400px',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {meals?.map((meal) => (
            <img
              alt="img"
              style={{ width: '200px', margin: '0 10px 0 10px' }}
              src={meal.image}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
