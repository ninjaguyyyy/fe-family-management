import React from 'react';
import Paper from '@material-ui/core/Paper';
import TrayChoice from './components/TrayChoice';
import TrayBoard from './components/TrayBoard';

export default function TrayContainer() {
    return (
        <div style={{ width: '68%' }}>
            <Paper elevation={3} style={{ padding: '20px 20px 20px 20px' }}>
                <TrayBoard />
                <TrayChoice />
            </Paper>
        </div>
    );
}
